#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>

#include <fcntl.h>
#include <poll.h>
#include <curl/curl.h>

struct Req {
	CURL	*handle;
	int	idx;
	bool	done;
	char	error[CURL_ERROR_SIZE];
	size_t	cap, p;
	char	*buf;
};

int	flush_buffers(int fd);
int	req_flush(int outfd, struct Req *req);
CURL*	req_init(struct Req*, const char *url);
size_t	req_write(char *data, size_t size, size_t len, void *udata);

CURLM *mhandle;
struct Req *reqs;
int creq;	/* index of request currently being written out */
int nreq;	/* number of requests */
int outfd = 1;

#define BLOCK_SIZE 16*1024

/* curl-cat: download given URLs and concatenate the results */
int
main(int argc, char* argv[])
{
	CURLMsg *m;
	int err, flags, flushed, i, mn;

	mhandle = curl_multi_init();
	if (!mhandle) {
		fprintf(stderr, "failed to initialize libcurl\n");
		return 1;
	}
	curl_multi_setopt(mhandle, CURLMOPT_MAX_HOST_CONNECTIONS, 4);

	flags = fcntl(outfd, F_GETFL);
	if (flags < 0) {
		perror("GETFL: output file descriptor error");
		return 1;
	}
	err = fcntl(outfd, F_SETFL, flags | O_NONBLOCK);
	if (err < 0) {
		perror("SETFL: output file descriptor error");
		return 1;
	}

	nreq = argc - 1;
	creq = 0;
	reqs = malloc(nreq * sizeof(*reqs));
	for (i = 0; i < nreq; i++) {
		CURL *h = req_init(reqs + i, argv[i + 1]);
		reqs[i].idx = i;
		if (!h) {
			fprintf(stderr, "failed to allocate libcurl request\n");
			return 1;
		}
		curl_easy_setopt(h, CURLOPT_PRIVATE, reqs + i);
		curl_multi_add_handle(mhandle, h);
	}

	while (creq < nreq) {
		CURLMcode merr;
		struct curl_waitfd cfd = {outfd, CURL_WAIT_POLLOUT, 0};
		int running;
		merr = curl_multi_wait(mhandle, &cfd, (reqs[creq].p > 0) ? 1 : 0, 1000, NULL);
		if (merr != CURLM_OK) {
			fprintf(stderr, "libcurl poll failed: %s\n", curl_multi_strerror(merr));
			break;
		}
		merr = curl_multi_perform(mhandle, &running);
		if (merr != CURLM_OK) {
			fprintf(stderr, "transfer failed: %s\n", curl_multi_strerror(merr));
			goto TEARDOWN;
		}

		while ((m = curl_multi_info_read(mhandle, &mn))) {
			struct Req *r;
			void *priv;
			CURLcode cerr;
			if (m->msg != CURLMSG_DONE)
				continue;
			cerr = curl_easy_getinfo(m->easy_handle, CURLINFO_PRIVATE, &priv);
			assert(cerr == CURLE_OK);
			r = priv;
			r->done = true;
			if (m->data.result) {
				fprintf(stderr, "%s\n", r->error);
				goto TEARDOWN;
			}
		}

		if (!running)
			break;
		flushed = flush_buffers(outfd);
		if (flushed < 0) {
			fprintf(stderr, "write failed: %s\n", strerror(-flushed));
			goto TEARDOWN;
		}
	}

	err = fcntl(outfd, F_SETFL, flags & ~O_NONBLOCK);
	if (err < 0) {
		perror("output file descriptor error");
		goto TEARDOWN;
	}

	do
		flushed = flush_buffers(outfd);
	while (flushed == 0);
	if (flushed < 0)
		fprintf(stderr, "write failed: %s\n", strerror(-flushed));
TEARDOWN:
	for (i = 0; i < nreq; i++) {
		if (reqs[i].handle) {
			curl_multi_remove_handle(mhandle, reqs[i].handle);
			curl_easy_cleanup(reqs[i].handle);
		}
	}
	curl_multi_cleanup(mhandle);
	
}

int
flush_buffers(int fd)
{
	struct Req *r;
	int err;
	while (creq < nreq) {
		r = reqs + creq;
		if (r->p > 0) {
			err = req_flush(fd, reqs + creq);
			if (err)
				return err;
			if (r->p > 0)
				return 0;
		}
		if (!r->done)
			return 0;
		curl_multi_remove_handle(mhandle, r->handle);
		curl_easy_cleanup(r->handle);
		r->handle = NULL;
		creq++;
	}
	return 1;
}

int
req_flush(int outfd, struct Req *req)
{
	ssize_t n;
	n = write(outfd, req->buf, req->p);
	if (n < 0) {
		if (errno == EAGAIN || errno == EWOULDBLOCK)
			return 0;
		return errno;
	}
	memmove(req->buf, req->buf + n, req->p - n);
	req->p -= n;
	return 0;
}

CURL*
req_init(struct Req *req, const char *url)
{
	CURL *h;
	h = curl_easy_init();
	if (!h)
		return NULL;
	curl_easy_setopt(h, CURLOPT_URL, url);
	curl_easy_setopt(h, CURLOPT_WRITEFUNCTION, req_write);
	curl_easy_setopt(h, CURLOPT_WRITEDATA, req);
	curl_easy_setopt(h, CURLOPT_ERRORBUFFER, req->error);
	curl_easy_setopt(h, CURLOPT_ACCEPT_ENCODING, "");
	memset(req, 0, sizeof(*req));
	req->handle = h;
	req->error[0] = 0;
	req->cap = req->p = 0;
	req->buf = NULL;
	req->done = false;
	return h;
}

size_t
req_write(char *data, size_t size, size_t len, void *udata)
{
	struct Req *req = udata;
	if (len > req->cap - req->p) {
		size_t ncap = req->cap;
		char *nb;
		while (len >= ncap - req->p)
			ncap += BLOCK_SIZE;
		nb = realloc(req->buf, ncap);
		if (!nb) {
			perror("failed to allocate buffer");
			return 0;
		}
		req->buf = nb;
		req->cap = ncap;
	}
	memmove(req->buf + req->p, data, len);
	req->p += len;
	return len;
}
