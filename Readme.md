Scripts to fetch your YouTube feed and automatically download videos and streams.
Copy `curl-cat` and `yt-record` into your `$PATH` and run `record-live` periodically.

## Configuration

- `$BROWSER`: optional, will be passed to `yt-dlp --cookies-from-browser` if set
- `$COOKIES`: optional, will be passed to `yt-dlp --cookies` if set
- `$RECORD_DIR`: download directory
- `$RECORD_DONE`: path to the file where IDs of downloaded or ignored videos will be appended.
- `$RECORD_MATCH`: `awk` pattern for videos that should be downloaded.
  Variables `id`, `url`, `chan`, and `title` are set to the video ID, video URL, channel name, and video title, respectively.
  `status` is set to the `yt-dlp` field `live_status`.  
  Default: `status == "is_upcoming" || status == "is_live"` (all scheduled and live streams)
- `$RECORD_SUBS`: path to the file listing subscribed channel IDs, one per line. Default: `~/.yt/subs`

You can extract channel IDs from [Google takeout](https://takeout.google.com/takeout/custom/youtube) data:

		cut -d, -f1 subscriptions.csv | sed 1d >~/.yt/subs

For example, to download all videos (but not streams) with "FNORD" in the title:

		export RECORD_MATCH='status == "not_live" && title ~ /FNORD/'

## Dependencies

- [libcurl](https://curl.se/libcurl/)
- [sfeed](https://codemadness.org/sfeed-simple-feed-parser.html)
- [yt-dlp](https://github.com/yt-dlp/yt-dlp)
